﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dragablz
{
    public interface IInterTabWindow
    {
        void DragStart();
        void DragStop();
    }
}
